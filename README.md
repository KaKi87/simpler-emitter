# `simpler-emitter`

27-lines (JSDoc excluded) simple & clean event emitter.

## `createEmitter()`

Create emitter

### Returns

Object

### `emit(event, data)`

Call listeners

#### Parameters

| Name  | Types  | Description |
| ----- | ------ | ----------- |
| event | string | Event name  |
| data  |        | Event data  |

### `off(event, listener)`

Remove listener

#### Parameters

| Name     | Types    | Description    |
| -------- | -------- | -------------- |
| event    | string   | Event name     |
| listener | function | Event listener |

### `on(event, listener)`

Add listener

#### Parameters

| Name     | Types    | Description    |
| -------- | -------- | -------------- |
| event    | string   | Event name     |
| listener | function | Event listener |

### `once(event, listener)`

Add one-time listener

#### Parameters

| Name     | Types    | Description    |
| -------- | -------- | -------------- |
| event    | string   | Event name     |
| listener | function | Event listener |

Documentation partly generated with [doxdox](https://github.com/docsbydoxdox/doxdox)