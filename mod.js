/**
 * Create emitter
 * @returns {{on: function, off: function, once: function, emit: function}}
 */
module.exports.createEmitter = () => {
    const
        listeners = {},
        /**
         * Add listener
         * @param {string} event - Event name
         * @param {function} listener - Event listener
         */
        on = (event, listener) => {
            if(!listeners[event])
                listeners[event] = [];
            if(!listeners[event].includes(listener))
                listeners[event].push(listener);
        },
        /**
         * Remove listener
         * @param {string} event - Event name
         * @param {function} listener - Event listener
         */
        off = (event, listener) => {
            const index = (listeners[event] || []).indexOf(listener);
            if(index !== -1)
                listeners[event].splice(index, 1);
        };
    return {
        on,
        off,
        /**
         * Add one-time listener
         * @param {string} event - Event name
         * @param {function} listener - Event listener
         */
        once: (event, listener) => {
            const _listener = data => {
                listener(data);
                off(event, _listener);
            };
            on(event, _listener);
        },
        /**
         * Call listeners
         * @param {string} event - Event name
         * @param [data] - Event data
         */
        emit: (event, data) => (listeners[event] || []).forEach(listener => listener(data))
    };
};